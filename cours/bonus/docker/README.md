# Docker

![Dockerzzzz](./pics/docker_logo.png)

Haaaaa la baleine 🐳. Docker est une technologie qui est présente partout aujourd'hui. Par partout on entend : surtout dans les offres d'emploi récentes, pour hipsters, et un peu dans l'industrie quand même aussi.

Jokes aside, Docker est devenu une techno que tout technicien en info, dév ou admin, se doit de maîtriser ; car cette techno a envahi les usages.

Des baleines pour le dév, des baleines pour la prod, des baleines pour faire des tests, des baleines pour packager. Les baleines. Partout. 🐳

![In the future](./pics/back-to-the-future-docker.jpg)

# Sommaire

- [Docker](#docker)
- [Sommaire](#sommaire)
- [I. C koa Docker](#i-c-koa-docker)
  - [1. Intro](#1-intro)
  - [2. Techniquement](#2-techniquement)
  - [3. Bénéfices apportés par Docker](#3-bénéfices-apportés-par-docker)
- [II. Se servir de Docker](#ii-se-servir-de-docker)
  - [1. Le concept d'image](#1-le-concept-dimage)
  - [2. docker run](#2-docker-run)
- [III. L'écosystème Docker](#iii-lécosystème-docker)
  - [1. Docker Hub](#1-docker-hub)
  - [2. docker-compose](#2-docker-compose)
  - [3. Un point sur la sécurité](#3-un-point-sur-la-sécurité)
- [IV. La ligne de commande Docker](#iv-la-ligne-de-commande-docker)
  - [1. docker run](#1-docker-run)
  - [2. docker images](#2-docker-images)
  - [3. docker ps](#3-docker-ps)
  - [4. docker build](#4-docker-build)
  - [5. docker exec](#5-docker-exec)
- [V. Cas d'utilisation](#v-cas-dutilisation)
  - [1. Créer des environnements de développement](#1-créer-des-environnements-de-développement)
  - [2. Livrer des applications](#2-livrer-des-applications)
  - [3. Faire tourner des applications en prod](#3-faire-tourner-des-applications-en-prod)
  - [4. Créer des environnements de test](#4-créer-des-environnements-de-test)
  - [5. Unifier l'outillage](#5-unifier-loutillage)

# I. C koa Docker

## 1. Intro

**🐳Docker est un outil qui permet d'exécuter une application.**

Plutôt que de juste l'exécuter en lançant simplement un exécutable, comme d'hab, **Docker va créer un environnement, isolé de la machine hôte, dans lequel s'exécutera l'application**.

On désigne cet environnement et l'application en cours d'exécution qu'il contient par le terme de ***conteneur*** (ou *container*).

> Notez le singulier : un *conteneur* n'exécute normalement **qu'une seule application**.

Dans cette idée, on peut faire l'analogie avec une VM. En effet une VM c'est aussi un environnement isolé de son hôte, dans lequel va s'exécuter plusieurs applications (un OS, un noyau, puis d'autres applications).  
**En cela on peut dire que les *conteneurs* et les VMs sont très similaires.**

> A partir d'ici nous ne parlerons que des *conteneurs* Docker comme ils sont fait traditionnellement sous GNU/Linux. Il existe pléthore de subtilités concernant la notion de *conteneur* mais c'est hors-sujet pour cette fois.

## 2. Techniquement

Techniquement, *conteneurs* et VMs sont assez différents.

➜ **Une VM a besoin de *virtualisation*.**  
C'est à dire que la machine qui porte des VMs doit émuler, pour chaque VM, un processeur, de la RAM, un disque, etc.  
Le but de la VM est vraiment d'émuler une machine physique, et la traiter comme telle : on va donc y installer un OS, se créer un utilisateur pour pouvoir se connecter, etc. L'utilisation classique d'un OS quoi !

Toutes les tâches effectuées dans une VM sont gérées par le noyau et l'OS de la VM, qui sont indépendants de ceux de son hôte.

---

➜ A l'inverse, **un *conteneur* ne fait appel à aucune *virtualisation*.**  
Rien n'est émulé. L'application qui est exécutée dans le *conteneur* est exécutée directement sur l'hôte.  
De l'isolation est mise en place : l'application n'a pas conscience d'être hébergée dans un *conteneur* ; mais c'est bel et bien directement le matériel, l'OS et le noyau de l'hôte qui sont utilisés par l'application conteneurisée.

![Container vs VM](./pics/containers-vs-virtual-machines.jpg)

➜ **Un *conteneur* exécute une application lorsqu'il se lance.**  
Par exemple un *conteneur* NGINX va lancer bah... `nginx`. Dès que cette application se termine, le *conteneur* meurt.

C'est **ESSENTIEL** à assimiler : lorsque le processus qu'on a demandé au *conteneur* d'exécuter meurt, le *conteneur* meurt. **Un *conteneur* c'est un processus en cours d'exécution.**

## 3. Bénéfices apportés par Docker

➜ **Isolation**

L'application exécutée dans le *conteneur* est isolée du système hôte. On parle ici de sécurité : l'application ne sait pas qu'elle est conteneurisée et ne peut pas accéder à l'hôte.  
L'application ne peut pas accéder aux fichiers de l'hôte, ni à ses processus, ni à ses cartes réseau, ni à rien en fait quoi :)

➜ **Environnement d'exécution**

Dans le *conteneur* s'exécute une application. Mais dans le *conteneur* se trouve aussi un environnement complet : tout un tas de fichiers (comme dans une VM, le *conteneur* possède sa propre arborescence de fichiers).

Ainsi, le *conteneur* possède tout ce dont l'application a besoin pour s'exécuter.

Plutôt que d'installer le nécessaire pour que l'application s'exécute sur l'hôte, on va tout installer à l'intérieur du *conteneur*.  L'hôte reste ainsi clean, sans avoir besoin d'y installer toutes les dépendances des applications.

**Un *conteneur* tient donc une application + tout le nécessaire pour que cette application puisse s'exécuter.**

![Ship your machine](./pics/ship_machine.jpg)

➜ **Légèreté**

Un *conteneur* est très léger, de l'ordre de quelques Mo. Aussi, il ne nécessite aucune virtualisation.  
Ainsi, lancer un nouveau *conteneur* ne prends que quelques secondes tout au plus.

Dit autrement, avec les conteneurs, **lancer un environnement complet, autonome et isolé de l'hôte, avec une application qui s'exécute dedans, cela prend quelques secondes tout au plus.**

➜ Facilité de mise à jour

Mettre à jour une application conteneurisée est assez simple. En effet : on ne met plus à jour. S'il faut mettre à jour, on détruit le *conteneur* et on le relance, dans la nouvelle version. C'est possible car le *conteneur* est très léger.

# II. Se servir de Docker

## 1. Le concept d'image

Une *image Docker* est à un *conteneur* ce qu'un *.iso.* est à une VM.

Pour créer une VM : on a besoin d'un fichier `.iso` qui permettra d'installer un OS dans la VM. Ainsi, à partir d'un `.iso` on peut créer plusieurs VMs : on peut instancier des VMs.

**Une *image Docker* permet d'instancier un *conteneur*.**

Une *image*, c'est par exemple un OS de base :

- Debian
- Ubuntu
- Alpine
- etc.

Ou directement une application, prête à être lancée :

- NGINX
- MySQL
- NextCloud
- etc.

## 2. docker run

Pour utiliser Docker, il suffit de l'installer sur une machine. Une fois que c'est fait, on lance le démon Docker et on accès aux commandes `docker` dans le terminal.

La commande élémentaire est la commande `docker run` qui permet de lancer un *conteneur* (=> d'instancier une *image*).

Exemple :

```bash
$ docker run debian
$ docker run nginx
```

Chaque ligne `docker run` lance un nouveau *conteneur*.  La commande `docker ps` permet de lister les *conteneurs* créés.

# III. L'écosystème Docker

## 1. Docker Hub

**🐳 Le Docker Hub est un répertoire public d'*images* Docker.**

Le Docker Hub fonctionne sur un principe analogue à Github ou Gitlab pour les dépôts Git : n'importe qui peut se créer un compte sur le Docker Hub et uploadé des *images*, qui seront alors accessibles publiquement.

Il existe des *images* dites "officielles" qui sont développées par les éditeurs de l'application en question.

Par exemple, l'*image* officielle Debian est une *image* Docker créée par les développeurs de chez Debian. Une *image* dite officielle nous donne aussi la garantie que l'équipe Docker a vérifié que l'*image* était conforme aux bonnes pratiques de conception d'*images*.

## 2. docker-compose

`docker-compose` est un outil pour faciliter l'utilisation de Docker.

Il a deux principales fonctions :

- lancer facilement plusieurs *conteneurs* simultanément
  - il n'est pas rare qu'une application ait besoin de plusieurs processus pour fonctionner
  - un serveur + une base de données par exemple
- remplacer de longues et répétitives commandes `docker run` par un fichier
  - on déclare les *conteneurs* à lancer dans un fichier `.yml`
  - et on les lance tous avec une unique commande `docker-compose`

Exemple de fichier `docker-compose.yml` qui permet de lancer deux *conteneurs* : un Apache et une base MySQL :

```yml
version: "3"

services:
  web:
    image: httpd
  db:
    image: mysql
```

![Go deeper](./pics/deeper.jpg)

## 3. Un point sur la sécurité

Les *conteneurs* mettent place de l'isolation, mais pas de virtu. Ainsi une machine virtuelle sera toujours plus sécurisée qu'un *conteneur* (il n'y a pas de débat là-dessus). L'isolation mise en palce par une VM est bien plus forte.

A l'inverse, la virtualisation est un procédé coûteux en performances. Donc un *conteneur* sera toujours plus performant qu'une VM ; idem, pas de débat ici.

Bon après... souvent... quand Docker est utilisé en prod, la machine qui lance les *conteneurs* Docker, c'est une VM. hihi.

> L'isolation mise en place par Docker est réalisée par le noyau Linux. L'isolation est fiable et forte, mais jamais infaillible.

![Docker security is fake news](./pics/docker_security_fake_news.jpg)

Pour clore ce rapide point sur la sécu : lancer une application conteneurisée sera toujours plus secure que de la lancer sans *conteneur* autour. On rajoute une couche d'isolation.

# IV. La ligne de commande Docker

## 1. docker run

➜ `-d` est très souvent utilisé sur `docker run` cela permet de lancer le *conteneur* en tâche de fond.

```bash
$ docker run -d nginx
```

➜ `-p` permet de partager un port de l'hôte vers un port du *conteneur*.  Ceci permet de rendre accessible l'application portée par le *conteneur* en utilisant l'IP de l'hôte pour le visiter.

```bash
$ docker run -d -p 8080:80 nginx

$ curl localhost:8080
```

➜ `-v` permet de donner au *conteneur* accès à un volume : c'est un dossier de l'hôte qui sera partagé entre le *conteneur* et l'hôte.

```bash
# Le dossier /tmp/share sur l'hôte et le dossier /share dans le *conteneur* sont les mêmes
$ docker run -d -p 8080:80 -v /tmp/share:/share nginx
```

➜ On rajoute parfois `-it` pour avoir une session interactive : idéal pour un shell  
On peut de plus spécifier en fin de ligne la commande qu'on souhaite que le *conteneur* exécute. Exemple :

```bash
$ docker run -it debian bash

# On a un shell dans le conteneur
$
```

## 2. docker images

La commande `docker images` permet de lister les *images* actuellement présentes sur la machine.

Elles pveuent avoir été construites localement, ou récupérées depuis un registre d'*image* comme le Docker Hub.

## 3. docker ps

La commande `docker ps` permet de lister les *conteneurs* créés.

Sans options, la commande liste uniquement les contneurs actifs.

On ajoute souvent `-a` pour obtenir aussi la liste des *conteneurs* arrêtés.

Parfois `-q` pour n'obtenir que les IDs des *conteneurs*.

## 4. docker build

La commande `docker build` permet de créer une nouvelle *image* customisée. Cela permet de créer des *images* sur mesure, afin de lancer des *conteneurs* exactement comme on le souhaite.

> Modifier ou créer des `.iso` est une tâche un peu plus fastidieuse. Avec les *images* Docker, c'est très simple.

`docker build` prend un fichier en entrée : le `Dockerfile`. Ce `Dockerfile` contient les instructions pour construire la nouvelle *image*.

Pour construire une *image*, généralement, on indique dans le `Dockerfile` :

- une *image* de base
  - on part d'une *image* existante et on la modifie
  - il est possible de créer une *image* à partir de rien, mais hors-sujet pour nous
- des commandes à exécuter à l'intérieur de l'*image*
  - pour qu'elle soit prête à l'emploi
- une comande à exécuter lorsqu'on lance le conteneur
  - pour rappel, un *conteneur* c'est juste une application qu'on exécute
  - donc dans l'*image*, on définit quelle application doit être lancée lorsqu'on lancera un *conteneur* issu de cette *image*

Exemple de `Dockerfile` qui ajout NGINX dans une *image* Debian, et qui lance un shell `bash` quand on l'instancie en *conteneur* :

```Dockerfile
FROM debian

RUN apt-get update -y

RUN apt-get install -y nginx

CMD [ "/bin/bash"]
```

Il est alors possible de construire l'image en lui donnant un nom (ici `test-nginx`) :

```bash
$ ls
Dockerfile

# Le . fait référence au dossier où se trouve le Dockerfile
# Ce dossier est appelé le "context" dans lequel on build l'image
$ docker build . -t testnginx
```

On pourra alors run notre *image* custom :

```bash
$ docker run -it testnginx

# Et ça lance le bash qu'on a mis en CMD dans le Dockerfile !
# Donc on a un shell bash dans le contneur direct :
$ 
```

## 5. docker exec

`docker exec` permet d'exécuter une commande dans un conteneur existant. Par exemple pour récupérer un shell dans un conteneur en cours d'exécution :

```bash
$ docker exec -it <CONTAINER_NAME> bash

# on récupère un shell bash dans le conteneur spécifié
$
```

# V. Cas d'utilisation

**Voyons quelques cas d'utilisation de 🐳Docker.**

## 1. Créer des environnements de développement

**Un *environnement de développement* c'est tous les bails qu'installe un développeur pour pouvoir développer sur sa machine.** Un IDE, un compilateur ou intepréteur pour son langage.  
Ca comprend aussi souvent un ou plusieurs frameworks et des tonnes de librairies.

Plutôt que d'installer tout ça sur son propre poste, le développeur peut utiliser un *conteneur* pour développer. Il peut créer une *image* pour chaque application sur laquelle il travaille.

Il suffit d'installer Docker (ou outil similaire) sur le poste de travail.

Au moment de lancer le *conteneur*, le dossier qui contient le code sur le PC du développeur est monté dans le *conteneur* à l'aide de l'option `-v`. Ainsi le développeur modifie le code sur son PC, et les changements sont aussi effectués dans le *conteneur*.

**C'est pratique, c'est rapide. Ca permet de pas pourrir sa propre machine ou encore de changer d'environnement très rapidement.**

![Everyone gets a dev environment](./pics/everyone_gets.jpg)

## 2. Livrer des applications

**Le *conteneur* embarque une application + toutes ses dépendances.** C'est **parfait** pour livrer une application plutôt que de faire de compiler des `.exe` avec plein de librairies dans tous les sens.

Ainsi il n'est plus rare de trouver aujourd'hui des applications que l'on peut télécharger soit traditionnellement sous forme d'un exécutable, un binaire ; soit sous forme d'une *image* Docker prête à l'emploi.

> Cela a l'inconvénient que les librairies embarquées dans le *conteneur* seront ptet en double avec celles du système, ou même d'autres *conteneurs*. C'est le prix de l'isolation.

## 3. Faire tourner des applications en prod

**Un environnement isolé ? Sécurité donc ? N'importe quelle application de prod, plutôt que d'être simplement lancée, elle peut être lancée dans un *conteneur* et ainsi apporter tous les bénéfices liés au *conteneur* dans l'environnement de prod.**

L'application est isolée, donc le niveau de sécurité de l'application augmente : si un hacker en prend le contrôle il sera bloqué et ne pourra pas remonter au système hôte.

Il est facile et rapide de déployer des nouvelles applications ou de les mettre à jour.

## 4. Créer des environnements de test

**Avant d'envoyer une application en production, on la teste.**  
C'est à dire, on l'exécute dans un environnement privé auquel les clients finaux n'ont pas accès.  
On vérifie son bon fonctionnement en simulant l'accès de client, en simulant une utilisation normale de l'application, et en simulant bien sûr aussi des comportements excessifs. Ceci pour s'assurer que l'application continue de fonctionner même si un client fout le dawa.

**Le *conteneur* est particulièrement adapté à cette situation.** Pour tester l'application : il suffit de la mettre dans une image, et d'instancier le *conteneur*. Le procédé est simple et rapide. Pas besoin d'initialiser une VM, dans laquelle on va installer toutes les dépendances, ce qui est un procédé plus fastidieux.  
Avec une image, les dépendances sont déjà dedans.

## 5. Unifier l'outillage

Dans les exemples donnés ci-dessus, du développement jusqu'à la production, en passant par les tests, l'application pourrait toujours être exécutée dans un *conteneur*.

Ainsi l'environnement d'exécution, du développement à la production est rigoureusement le même tout le long.

Aussi, les développeurs comme les admins utilisent le même outil pour mettre en place cela : un outil de conteneurisation comme Docker.

![Docker evrywhere](./pics/everywhere.jpeg)
