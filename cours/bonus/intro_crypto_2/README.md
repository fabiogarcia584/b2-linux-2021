# Intro Crypto 2

⚠️ **Il est nécessaire d'avoir assimilé les notions de [la première partie de l'intro crypto](../intro_crypto/README.md).**

Deux objectifs dans ce cours bonus :

- dérouler l'algo RSA manuellement
  - chiffrement
  - déchiffrement
  - génération des clés
- démystifier TLS
  - le `s` de `https` entre autres
  - et par ce biais, expliquer la notion de certificats

> *Well. Go grab a cup of tea, relax, and enjoooy.*

![Prime nuumbeeeers](pics/prime-numbers.jpg)

# Sommaire

- [Intro Crypto 2](#intro-crypto-2)
- [Sommaire](#sommaire)
- [I. RSA](#i-rsa)
  - [1. Intro](#1-intro)
  - [2. Génération de clés](#2-génération-de-clés)
    - [A. Choisir deux premiers](#a-choisir-deux-premiers)
    - [B. Calculer N](#b-calculer-n)
    - [C. Calculer Φ(N)](#c-calculer-φn)
    - [D. Choisir e](#d-choisir-e)
    - [E. Choisir d](#e-choisir-d)
    - [F. SO WHAT NOW](#f-so-what-now)
  - [3. Chiffrement](#3-chiffrement)
  - [4. Déchiffrement](#4-déchiffrement)
  - [5. La force de RSA](#5-la-force-de-rsa)
  - [6. Commandes pour générer des clés](#6-commandes-pour-générer-des-clés)
- [II. TLS](#ii-tls)
  - [1. Intro](#1-intro-1)
    - [A. Définition](#a-définition)
    - [B. Concrètement](#b-concrètement)
    - [C. Un mot sur SSL](#c-un-mot-sur-ssl)
    - [D. A quoi ça sert](#d-a-quoi-ça-sert)
    - [E. Le cas HTTPS](#e-le-cas-https)
  - [2. L'échange TLS](#2-léchange-tls)
    - [Step 1 : négociation](#step-1--négociation)
    - [Step 2 : échange de clés asymétriques](#step-2--échange-de-clés-asymétriques)
    - [Step 3 : échange de clé symétrique](#step-3--échange-de-clé-symétrique)
- [III. Les certificats](#iii-les-certificats)
  - [1. Le contenu du certificat](#1-le-contenu-du-certificat)
  - [2. CA](#2-ca)
    - [A. Le magasin de certificats](#a-le-magasin-de-certificats)
    - [B. La CA](#b-la-ca)

# I. RSA

## 1. Intro

**[RSA](https://en.wikipedia.org/wiki/RSA_(cryptosystem)) est le nom d'un algorithme de chiffrement asymétrique** (voir [le premier cours intro crypto](../intro_crypto/README.md) pour + de détails sur la notion de *chiffrement asymétrique*).

Il repose en grande partie sur deux concepts mathématiques :

- arithmétique modulaire
  - l'opérateur modulo : `%`
  - aussi noté `(mod x)`
  - genre `17 % 4` pareil que `17 (mod 4)`
- nombres premiers

**RSA est encore aujourd'hui considéré comme un algorithme de chiffrement robuste**, si l'on utilise des clés assez longues (longueur de clé >= 4096 bits).

Il est notamment utilisé dans certaines connexions [TLS](#ii-tls) ou encore pour des connexions SSH (liste non-exhaustive, mais alors pas du tout du tout hihi).

## 2. Génération de clés

**La génération de clés se fait en 5 étapes successives.**

### A. Choisir deux premiers

**La première étape consiste à choisir aléatoirement deux nombres premiers.**

Dans un cas réel, les nombres choisis sont énooooormes (1000+ digits).  
Dans notre cas, on va choisir des petits nombres, pour faciliter les calculs, le principe reste le même.  

On choisit 2 et 7 pour l'exemple

```math
p=2 ; q=7
```

> Vous noterez l'utilisation de l'aléatoire, qui pose des problèmes en informatique, comme vous le savez.

### B. Calculer N

`N` est le simple produit de `p` et `q`.

```math
p=2 ; q=7
```

```math
N = p * q
```

```math
N = 2 * 7
```

```math
N = 14
```

### C. Calculer Φ(N)

`Φ(N)` (prononcé "phi de N") est l'application de la fonction `Φ` sur le nombre `N`.

La fonction `Φ` est le nombre de *copremiers* à N entre 1 et N.  

On dit que deux nombres sont "*copremiers*" quand ils ne partagent pas de facteur premier.  

Par exemple :

- on dit que 10 et 6 ne sont pas *copremiers* car
  - 10 = 5 * 2
  - 6 = 3 * 2
  - 2 est un nombre premier
  - 2 est un facteur à la fois de 10 et 6
- on dit que 14 et 15 sont *copremiers* car
  - 14 = 7 * 2
  - 15 = 5 * 3
  - ils ne partagent pas de facteurs

> Il a été prouvé mathématiquement qu'il existe une seule et unique décomposition d'un entier en facteurs premiers. Il n'existe qu'une seule façon de décomposer 14 : c'est 7 * 2.

---

**Plutôt que de les compter simplement** (surtout quand `p` et `q` sont très grands), **on peut facilement calculer `Φ(N)` grâce à la formule `Φ(N) = (p-1)(q-1)`**.

```math
p=2 ; q=7
```

```math
N = 14
```

```math
Φ(N) = (p-1)(q-1)
```

```math
Φ(N) = (2-1)(7-1)
```

```math
Φ(N) = (1)(6)
```

```math
Φ(N) = 6
```

### D. Choisir e

On choisit `e` selon deux conditions :

- `1 < e < Φ(N)`
- `e` doit être *copremier* avec `N` et `Φ(N)`

Appliqué à notre cas, on a :

- `1 < e < 6`
- `e` doit être *copremier* avec `14` et `6`

Selon la première condition, les seuls choix possibles sont 2, 3, 4 et 5.

Pour la deuxième condition, testons chacun des choix possibles, afin de voir s'ils sont *copremiers* avec `14` ou `6` :

- 2 est premier, et c'est un facteur de 14 et 6
- 3 est premier, et c'est un facteur de 6
- 4 n'est pas premier
  - 4 = 2 * 2
  - 2 est un facteur de 14 et 6
- 5 est premier, et n'est pas un facteur ni de 14, ni de 6

On a donc `e = 5`.

```math
p = 2 ; q = 7
```

```math
N = 14
```

```math
Φ(N) = 6
```

```math
e = 5
```

### E. Choisir d

`d` est donné par la relation `(d * e) % Φ(N) = 1`

Si on plug nos valeurs actuelles, ça donne : `(d * 5) % 6 = 1`.

'a pu qu'à résoudre l'équation. On obtient `d = 5` ou `d = 11` ou même `d = 17` etc. (plusieurs réponses possibles, normal, on applique un modulo).

On ne choisit pas `d = 5` car c'est déjà la valeur de `e`. Pour la simplicité de calcul, on prend la valeur qui suit.

```math
p = 2 ; q = 7
```

```math
N = 14
```

```math
Φ(N) = 6
```

```math
e = 5
```

```math
d = 11
```

### F. SO WHAT NOW

**Bah maintenant on a tout ce qu'il nous faut : on a nos deux clés.** Tout ce que chiffre une clé, l'autre peut le déchiffrer.

Pour l'exemple de chiffrement et déchiffrement, on va utiliser :

- `(e,N)` pour chiffrer (`e` comme *encryption*)
  - chez nous `(5,14)`
- `(d,N)` pour déchiffrer (`d` comme *decryption*)
  - chez nous `(11,14)`

## 3. Chiffrement

Pour la simplicité des calculs, on va chiffrer un message TRES simple : "B". En considérant que B est la deuxième lettre de l'alphabet, on dira que B = 2.

Notre clé de chiffrement `(e,N)` c'est `5,14`

On veut donc chiffrer `2` avec la clé `5,14` afin d'obtenir le message chiffré (ou *ciphertext*, souvent abrégé *ctext*).

La formule du chiffrement c'est `input^e % N` (qu'on peut aussi noter `input^e (mod N)`). Let's go :

```math
e = 5
```

```math
N = 14
```

```math
input = 2
```

```math
ctext = input ^ e \pmod{N}
```

```math
ctext = 2 ^ 5 \pmod{14}
```

```math
ctext = 4
```

**`4` qui correspond à "D" est notre message chiffré.**

> Gardez à l'esprit que les nombres choisis dans la vraie vie sont gigantesques, et les messages sont rarement constitués d'une seule lettre.

## 4. Déchiffrement

Notre clé de déchiffrement `(d,N)` c'est `11,14`

La formule du déchiffrement c'est `ctext^d % N`. Let's go :

```math
d = 11
```

```math
N = 14
```

```math
ctext = 4
```

```math
text = ctext ^ d \pmod{N}
```

```math
text = 4^{11} \pmod{14}
```

```math
txt = 4194304 \pmod{14}
```

```math
text = 2
```

**On retrouve bien `text = 2` soit "B" notre message original.**

## 5. La force de RSA

**REMARQUE TRES IMPORTANTE** : en ayant choisi `p = 2` et `q = 7` à la base, on passe, pour le déchiffrement, par une étape avec `4194304 % 14`. Vous imaginez la taille de ce calcul, quand, dès le départ, `p` et `q` sont constitués de plusieurs milliers de chiffres ?

**Et c'est ça la solidité de RSA.** En choisissant des nombres premiers si grands (même si la liste des nombres premiers est connue jusqu'à de trèèèès grandes valeurs), on s'assure que personne est capable de remonter aux `p` et `q` qu'on a choisi à la base.

Pour rappel, `N` est dans les deux clés. Donc dans un cas réel, `N` est dans la clé privée comme dans la clé publique. Autrement dit, un hacker possède `N` et `d` (la clé publique) et il cherche à trouver `e` (le constituant de la clé privée manquant).

Hors `e` et `d` sont tous les deux donnés par de simples relation de multiplication.

La force de RSA c'est donc que personne n'est capable de décomposer de très trèèèèèès grands nombres en facteurs premiers.

**On considère RSA 'sûr' car même avec une énorme puissance de calcul** *(du genre 50% de la puissance de calcul mondiale, c'est souvent la valeur fictive utilisée pour raisonner en crypto)* **ça prendrait plusieurs dizaines d'années à trouver `e` en connaissant `d` et `N`.**

> Du moment que les valeurs choisies sont très grandes, encore une fois. Valeur recommandées pour la taille de la clé aujourd'hui : 4096 bits.

![tooo looooong](./pics/crypto-key-generate-rsa-modulus-2048.jpg)

## 6. Commandes pour générer des clés

Commandes pour générer des clés RSA considérées comme sûres à la date de ce document. C kdo 🎁.

```bash
# Clés RSA simples, format standard
# Prêtes pour SSH par exemple
$ ssh-keygen -t rsa -b 4096

# Une clé et un certificat, forma standard
# Pour un site Web par exemple (genre *PAP* dans Apache ou NGINX, et *BOOM* du HTTPS)
# Ils seront générés dans le dossier courant
$ openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -keyout server.key -out server.crt
```

# II. TLS

⚠️ **Il est strictement nécessaire** d'avoir assimilé les notions de [signature](../intro_crypto/README.md#a-signature) et d'[échange confidentiel](../intro_crypto/README.md#b-echange-confidentiel) abordées dans [la première partie de l'intro crypto](../intro_crypto/README.md).

## 1. Intro

### A. Définition

**[TLS](https://fr.wikipedia.org/wiki/Transport_Layer_Security) est un protocole permettant l'établissement d'un *tunnel chiffré* entre deux entités** (deux machines quoi).

**Le terme *tunnel* c'est juste une image.**  

**Une fois que deux entités se sont mises d'accord** sur un algo de chiffrement à utiliser, et qu'elles ont échangé les clés nécessaires, **elles peuvent communiquer de façon sécurisée** (confidentialité + signature de leurs messages).  

**Une fois cette relation établie, on dit qu'il y a un *tunnel* établi entre les deux entités.**  
*"Envoyer des messages dans ce tunnel"* c'est simplement utiliser l'algo et les clés sur lesquelles on s'est mis d'accord.

### B. Concrètement

**Concrètement, TLS est un algorithme qui décrit la suite des opérations qu'un client et un serveur doivent effectuer pour établir un tunnel chiffré.**  
Ca implique du chiffrement symétrique, du chiffrement asymétrique, du hashing, et des techniques comme l'échange Diffie-Hellman.

**Quels algos de chiffrement ? Quelles fonctions de hachage ? Ca, TLS ne le précise pas, et il existe donc plusieurs façon de faire du TLS.**

Pour le chiffrement asymétrique, on utilise souvent RSA ou EC (Elliptical Curve). Ce dernier étant plus moderne, probable remplaçant de RSA, car plus robuste pour des clés de taille équivalente, et plus économe côté performances.

Pour le chiffrement symétrique, c'est souvent de l'AES, dans l'une de ses nombreuses déclinaisons.

Pour le hashing, du SHA1 ou SHA256 généralement.

### C. Un mot sur SSL

Vous avez peut-être déjà vu ce terme. C'est juste le vieux frère obsolète de TLS.  

Y'a eu SSL 1, puis SSL 2, puis SSL 3. Puis les gars se sont dit "meh, ce nom est pourri". Alors SSL 4 est devenu TLS 1.0.

Aujourd'hui il existe plusieurs versions de TLS : 1.0, 1.1, 1.2 et 1.3.

**On considère que seuls TLS 1.2 et TLS 1.3 sont d'actualité. Tout le reste est obsolète.**

### D. A quoi ça sert

**TLS permet donc l'établissement d'un tunnel entre deux machines, généralement un client vers un server. Il résout plusieurs problèmes :**

- comment on se met d'accord sur un algo à utiliser ?
- comment on échange les clés pour chiffrer nos messages ?
- comment le serveur prouve son identité au client ?
- **et ce, sans avoir jamais communiqué auparavant, donc tout va circuler en clair au début**

> Bah oui, on cherche justement à chiffrer tous nos échanges, donc on a rien encore au début, tout doit passer en clair, pas le choix.

### E. Le cas HTTPS

**TLS c'est le `s` de `https`.** Les exemples qui suivront seront donnés dans le cas de `https`. Car oui, on peut utiliser TLS pour chiffrer n'importe quel type d'échange :)

Avec `https`, on est précisément dans ce cas : on veut établir une connexion sécurisée avec un site où potentiellement, on est jamais allés avant. Donc il faut échanger des clés de façon sécurisée.  
**Pour des questions de performances, il faut des clés symétriques** dans le cas du web (pour du trafic `http` et les besoins du 21ème siècle).

> On estime que TLS est responsable de + de 75% des ressources consommées lorsqu'on visite un site en `https`.

![what's goiiiing oooooooooon](./pics/http-secure-https-whats-going-on-12572187.png)

## 2. L'échange TLS

*Passons au vif du sujet.*

**Un client cherche à se connecter à un serveur**, et veut établir une communication sécurisée, **grâce à TLS**, avant d'accéder à ce que le serveur propose.

> Autrement dit, vous tapez `https://www.youtube.com`.  
> En effet, taper ça dans votre navigateur revient à dire "je veux établir une connexion TLS sécurisée vers le serveur `www.youtube.com` sur le port `443/tcp` afin de parler avec lui en HTTP".

**On va décortiquer TLS en 3 grosses étapes. C'est de la vulgarisation, TLS est évidemment plus précis que ça.**

> *[Lien vers un très bon site opensource et libre](https://tls.ulfheim.net/)* qui décrit de façon exhaustive la liste des échanges TLS, leur utilité et leur contenu.

### Step 1 : négociation

**Le client et le serveur vont entamer une *négociation*.**

➜ **D'un côté, le client. Il fait ce qu'il veut**, ça peut potentiellement être :

- **Alice**, qui vient avec son navigateur Firefox dernière mise à jour, tout beau tout propre
- **Bob**, qui vient avec son Internet Explorer 9
- **Eve**, qui vient éclater ton site. Vilaine Eve. Vilains les hackers.

➜ **De l'autre, le serveur. C'est un admin *éclairé* qui le contrôle.**  
Il a configuré son site Web pour n'accepter que les connexions avec certaines versions de TLS, avec des algos de chiffrements forts (*gros EC sa maman avec des clés longues comme mon bras*).  
Maiiiis avec un peu de compatibilité pour des navigateurs pas complètement à jour.

➜ **La négociation c'est le client et le serveur qui vont choisir quels algos utiliser par la suite.**

> Le serveur tire vers le haut en terme de robustesse sur le choix des algos. Et les clients parfois vers le bas (**Bob** et **Eve** tirent vers le bas).

### Step 2 : échange de clés asymétriques

Une fois qu'ils se sont mis d'accord, le serveur fournit son ***certificat*** au client.

Un certificat contient plein d'infos. **L'info la plus importante du certificat c'est la clé publique du serveur.**

**Quand un serveur vous donne son certificat, il vous donne sa clé publique.**

➜ Une fois que vous avez la clé publique du serveur, il va être en mesure de vous envoyer des messages signés, et vous pourrez lui envoyer des messages confidentiels.

➜ Vous lui donnez alors votre clé publique, que vous avez généré pour l'occasion. Afin que vous puissiez vous aussi signer vos messages, et que le serveur puisse vous envoyer des messages confidentiels.

> Pour bien comprendre les deux derniers points, encore une fois, c'est strictement nécessaire d'avoir assimilé les notions de [signature](../intro_crypto/README.md#a-signature) et d'[échange confidentiel](../intro_crypto/README.md#b-echange-confidentiel) abordées dans [la première partie de l'intro crypto](../intro_crypto/README.md).

### Step 3 : échange de clé symétrique

La dernière étape consiste à **utiliser ce tunnel de chiffrement asymétrique** établi en *Step 2* juste au dessus **pour échanger des clés symétriques**.

**En effet, pour rappel**, bien qu'aussi robustes, les chiffrements symétriques sont nettement moins coûteux en performances que les chiffrements asymétriques.

Et une fois que c'est fait, c'est tipar... ou presque :(

# III. Les certificats

> Ouais, ça mérite une partie entière.

**WAIT WAIT WAIT** c'est pas fini. Y'a un **GROS** problème là. Rappelons les problématiques que [*TLS*](#ii-tls) est censé résoudre, énoncées plus haut :

- comment on se met d'accord sur un algo à utiliser ?
- comment on échange les clés pour chiffrer nos messages ?
- comment le serveur prouve son identité au client ?
- **et ce, sans avoir jamais communiqué auparavant, donc tout va circuler en clair au début**

Dans [la section TLS](#ii-tls), on a vu comment ils se mettaient d'accord, et comment ils échangeaient des clés.

**Le serveur n'a jamais prouvé son identité au client, et ça c'est une autre fonction du certificat.** Sinon, il lui filerait juste sa clé publique *enfet*. ENFET. Eh ui. Eh ui eh ui.

## 1. Le contenu du certificat

Donc les autres infos importantes que contient un certificat :

- **une durée de validité**
- **le nom de domaine pour lequel le certificat est valide**
- **la signature d'une *CA***

---

➜ **Une durée** OK.

➜ **Le nom de domaine**, c'est logique : le but du serveur en présentant son certificat, c'est de vous prouver son identité. Genre "je suis bien google.com oui c'est bien moi". Donc on trouve *évidemment* dans le certificat le nom pour lequel ledit certificat est valide.

> Un certificat sans nom de domaine c'est comme une carte d'identité, mais sans le prénom ni le nom. Ca sert à R frer.

➜ **La signature d'une *CA*.** Pour la *signature*, encore une fois, voir le [premier cours bonus d'intro crypto](../intro_crypto/README.md).  
La notion de *CA* mérite sa partie.

## 2. CA

![RESPECT MY CA](./pics/respect-my-certificate-authority.jpg)

***CA* c'est pour "*certificate authority*" ou "*autorité de certification*".** C'est des gens qui se posent là, comme ça. Et nous, on leur fait confiance. Développons.

### A. Le magasin de certificats

**Dès que vous installez un OS ou un navigateur, il est fourni avec un *magasin de certificats*.** C'est simplement un ensemble de *certificats* fournis à l'intérieur du logiciel. Genre pas le choix.  

**Ces certificats appartiennent à des acteurs connus, admis comme étant de confiance.**

> On y trouve pêle-même Amazon, Google, Digicert, etc. Ce magasin est parfaitement consultable, aussi bien celui de votre OS que de votre navigateur. [Random article qui vous montre comment y accéder (et même le modifier) sur Firefox](https://docs.vmware.com/en/VMware-Adapter-for-SAP-Landscape-Management/2.0.1/Installation-and-Administration-Guide-for-VLA-Administrators/GUID-0CED691F-79D3-43A4-B90D-CD97650C13A0.html).

**Dans ce contexte, on appelle ces acteurs des *CA*.**

### B. La CA

**On appelle *CA* une entité qui va signer les certificats des autres.**

La *CA* va signer **(= chiffrer avec sa clé privée)** le certificat de celui qui en fait la demande.  
L'admin d'un serveur web par exemple, parce qu'il veut que son site soit en HTTPS le bougre.

Vu que tout le monde (les clients), dans son OS et/ou dans son navigateur possède le certificat de cette *CA* **(= la clé publique de cette *CA*)**.

Et bien **tout le monde peut attester que quelqu'un de réputé (une *CA*) a bien signé le certificat du serveur.** La signature quoi !

**La *CA*, en signant le certificat de quelqu'un d'autre, atteste que ce certificat est digne de confiance** (au moins autant que la confiance que vous accordez à cette *CA*).

Les amis de mes amis sont mes amis. On appelle ça une chaîne de confiance ou *trust chain*. **Vous faites confiance à la *CA*, donc vous faites confiance au serveur.**

**La clé de voûte du machin : c'est la *CA* qui s'assure de son côté qu'une seule personne peut demander un unique certificat pour un nom de domaine donné sur une période donnée.**

> Il est parfaitement possible de se faire une petite *CA* maison, et d'installer son certificat dans votre navigateur ou dans votre OS. Mais bon, y'aura que vous pour trust les certificats que votre *CA* signera. En entreprise ça se fait pas mal, une *CA* en interne, pour les applications en interne.

---
---

🔥 **Baboom.** Vous pouvez trust le serveur s'il y a un cadenas vert à côté de HTTPS.

> Because big prime numbers. Hihi.

![I should hack a CA](./pics/ishouldhack.jpg)